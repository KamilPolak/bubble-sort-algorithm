#Implementation of bubble sort algorithm

def bubble_sort(arr):
    #get the lenghts of the list
    n = len(arr)

    for i in range(n-1):
        # inner loop that compares all the values in the list from the first to the last on
        for j in range(n-1):
            # check if the value on the left-hand side is greater than the one on the immediate right side
            if arr[j] > arr[j+1]:
                #replace the numbers
                arr[j], arr[j+1] = arr[j+1], arr[j]
    print(arr)



arr = [3,8,4,9,2]

bubble_sort(arr)
