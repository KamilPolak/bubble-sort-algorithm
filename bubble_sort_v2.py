#Implementation of bubble sort algorithm
   

def bubble_sort(arr):
    #get the lenghts of the list
    n = len(arr)

    # to check all unique list elements
    flag = len(set(arr)) == len(arr)
    
    if n == 0:
        return "List is empty. There is nothing to sort"  
    
    if(not flag):
        return "List does not contains all unique elements"
    
    
    for i in range(n-1):
        for j in range(n-1):
            if arr[j] > arr[j+1]:
                #replace the numbers
                arr[j], arr[j+1] = arr[j+1], arr[j]
           
                
           
    return arr

                


    
                
arr = [1,2,6,5,9,3,4]


print(bubble_sort(arr))
